import 'package:flutter/material.dart';

class ItemTile extends StatelessWidget{
  final String title;
  final String? subtitle;
  final String icon;
  final Function()? ontap;
  const ItemTile({super.key, required this.title, this.subtitle, required this.icon, this.ontap});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(color: Color(0x26000026), offset: Offset(0, 2))
          ]
      ),
      child: ListTile(
        contentPadding: EdgeInsets.symmetric(horizontal: 12),
        leading: Image.asset(icon),
        title: Text(title,
          style: Theme.of(context).textTheme.labelLarge?.copyWith(color: Color(0xFF3A3A3A))),
        subtitle: (subtitle != null) ? Text(subtitle!, style: Theme.of(context).textTheme.labelSmall,): null,
        trailing: const ImageIcon(AssetImage("assets/vector.png")),
        onTap: (ontap != null) ? ontap!: null,
      ),
    );

  }

}
