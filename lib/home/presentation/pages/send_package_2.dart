import 'package:flutter/material.dart';
import 'package:sec_2_6/home/presentation/pages/delivery_successful.dart';
import 'package:sec_2_6/home/presentation/pages/transaction_successful_1.dart';


import '../widgets/text_field.dart';

class Send_package_2_Page extends StatefulWidget {
  final String id;
  final List destinations;
  final String address;
  final String country;
  final String phone;
  final String others;
  final String items;
  final String weight;
  final String worth;
  final String delivery_charges;
  final String instantDelivery;
  final String tax;
  final String sum_price;

  const Send_package_2_Page({super.key, required this.id, required this.destinations, required this.address, required this.country, required this.phone, required this.others, required this.items, required this.weight, required this.worth, required this.delivery_charges, required this.instantDelivery, required this.tax, required this.sum_price});


  @override
  State<Send_package_2_Page> createState() => _Send_package_2_PageState();
}

class _Send_package_2_PageState extends State<Send_package_2_Page> {
  String destinationText = '';


  @override
  void initState() {
    super.initState();
    var elements = widget.destinations.map(
            (e) => e["address"] + ", " + e["country"] + "\n" + e["phone"]
    ).toList();
    var elements_with_index = [];
    for (int index = 0; index < elements.length; index++){
      elements_with_index.add("${index+1}. ${elements[index]}");
    }
    destinationText = elements_with_index.join("\n");
  }
  Widget build(BuildContext context) {

    return Scaffold(
        body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 108,
                width: double.infinity,
                alignment: Alignment.bottomLeft,
                padding: const EdgeInsets.only(left: 14, right: 14, bottom: 19),
                decoration: const BoxDecoration(color: Colors.white, boxShadow: [
                  BoxShadow(
                      color: Color(0x26000026), blurRadius: 5, offset: Offset(0, 2))
                ]),
                child: Stack(children: [
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Text("Send a package",
                        style: Theme.of(context).textTheme.labelLarge),
                  ),
                  Align(alignment: Alignment.bottomLeft,
                      child: InkWell(
                        onTap: (){
                          Navigator.of(context).pop();
                        },
                        child: Image.asset("assets/arrow-square-right.png"),
                      ))
                ]),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 24),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 27,),
                    Text("Package Information", style:Theme.of(context).textTheme.labelLarge?.copyWith(color: Color(0xFF0560FA)),),
                    SizedBox(height: 8,),
                    Text("Origin details", style: Theme.of(context).textTheme.labelSmall?.copyWith(color: Color(0xFF3A3A3A)),),
                    SizedBox(height: 4,),
                    Row(
                      children: [
                        Text(
                            widget.address,
                            style: Theme.of(context).textTheme.labelSmall),
                        Text(
                          widget.country,
                            style: Theme.of(context).textTheme.labelSmall
                        )

                      ],
                    ),
                    SizedBox(height: 4,),
                    Text(
                      widget.phone,
                        style: Theme.of(context).textTheme.labelSmall
                    ),
                    SizedBox(height: 8,),
                    Text("Destination details", style: Theme.of(context).textTheme.labelSmall?.copyWith(color: Color(0xFF3A3A3A)),),
                    Text(destinationText, style: Theme.of(context).textTheme.labelSmall),
                    SizedBox(height: 8,),
                    Text("Other details", style: Theme.of(context).textTheme.labelSmall?.copyWith(color: Color(0xFF3A3A3A)),),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                            Text(
                              "Package Items",
                                style: Theme.of(context).textTheme.labelSmall
                            ),
                            Text(
                                widget.items,
                                style: Theme.of(context).textTheme.labelSmall?.copyWith(color: Color(0xFFEC8000))
                            )
                      ],
                    ),
                    SizedBox(height: 7,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                            "Weight of items",
                            style: Theme.of(context).textTheme.labelSmall
                        ),
                        Text(
                            widget.weight,
                            style: Theme.of(context).textTheme.labelSmall?.copyWith(color: Color(0xFFEC8000))
                        )
                      ],
                    ),
                    SizedBox(height: 7,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                            "Worth of Items",
                            style: Theme.of(context).textTheme.labelSmall
                        ),
                        Text(
                            widget.worth,
                            style: Theme.of(context).textTheme.labelSmall?.copyWith(color: Color(0xFFEC8000))
                        )
                      ],
                    ),
                    SizedBox(height: 7,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                            "Tracking Number",
                            style: Theme.of(context).textTheme.labelSmall
                        ),
                        Text(
                            "R-${widget.id}",
                            style: Theme.of(context).textTheme.labelSmall?.copyWith(color: Color(0xFFEC8000))
                        )
                      ],
                    ),
                    SizedBox(height: 37,),
                    Divider(

                      color: Colors.black,
                    ),
                    SizedBox(height: 8,),
                    Text("Charges", style:Theme.of(context).textTheme.labelLarge?.copyWith(color: Color(0xFF0560FA)),),
                    SizedBox(height: 10,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                            "Delivery Charges",
                            style: Theme.of(context).textTheme.labelSmall
                        ),
                        Text(
                            widget.delivery_charges,
                            style: Theme.of(context).textTheme.labelSmall?.copyWith(color: Color(0xFFEC8000))
                        )
                      ],
                    ),
                    SizedBox(height: 7,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                            "Instant delivery",
                            style: Theme.of(context).textTheme.labelSmall
                        ),
                        Text(
                            "${widget.instantDelivery}",
                            style: Theme.of(context).textTheme.labelSmall?.copyWith(color: Color(0xFFEC8000))
                        )
                      ],
                    ),
                    SizedBox(height: 7,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                            "Tax and Service Charges",
                            style: Theme.of(context).textTheme.labelSmall
                        ),
                        Text(
                            "${widget.tax}",
                            style: Theme.of(context).textTheme.labelSmall?.copyWith(color: Color(0xFFEC8000))
                        )
                      ],
                    ),
                    SizedBox(height: 9,),
                    Divider(

                      color: Colors.black,
                    ),
                    SizedBox(height: 4,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                            "Package total",
                            style: Theme.of(context).textTheme.labelSmall
                        ),
                        Text(
                            "${widget.sum_price}",
                            style: Theme.of(context).textTheme.labelSmall?.copyWith(color: Color(0xFFEC8000))
                        )
                      ],
                    ),
                    SizedBox(height: 46,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                          SizedBox(
                              width: 158,
                              height: 48,
                            child: OutlinedButton(

                                style: ButtonStyle(
                                  side: MaterialStateProperty.all(
                                      BorderSide(
                                        color: Color(0xFF0560FA),
                                        width: 1.0,
                                        style: BorderStyle.solid,)),
                                  backgroundColor: MaterialStatePropertyAll<Color>(Colors.white,),
                                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(8),
                                          side: BorderSide(width: 1, color: Color(0xFF0560FA))
                                      )
                                  ),
                                ),
                                onPressed: (){
                                  Navigator.of(context).pop();
                                },
                                child: Text("Report", style: TextStyle(
                                    fontSize: 16,
                                    color: Color(0xFF0560FA),
                                    fontWeight: FontWeight.w700
                                ),))),
                          SizedBox(
                            width: 158,
                            height: 48,
                            child: OutlinedButton(
                                style: ButtonStyle(
                                  backgroundColor: MaterialStatePropertyAll<Color>(Color(0xFF0560FA),),
                                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(8),
                                      )
                                  ),
                                ),
                                onPressed: (){
                                  Navigator.push(context, MaterialPageRoute(builder: (context) => Delivery(id: widget.id)));

                                },
                                child: Text("Successful", style: TextStyle(
                                    fontSize: 16,
                                    color: Colors.white,
                                    fontWeight: FontWeight.w700
                                ),)),)
                      ],
                    )



                  ],
                ),

              )

            ],
          )),
    );
  }
}
