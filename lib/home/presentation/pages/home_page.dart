import 'package:flutter/material.dart';
import 'package:sec_2_6/home/data/repository/supabase.dart';
import 'package:sec_2_6/home/presentation/pages/delivery_successful.dart';
import 'package:sec_2_6/home/presentation/pages/profile_page.dart';
import 'package:sec_2_6/home/presentation/pages/send_package.dart';
import 'package:sec_2_6/home/presentation/pages/tracking_package.dart';
import 'package:sec_2_6/home/presentation/pages/wallet.dart';

import 'notification.dart';

class MyHomePage extends StatefulWidget {
  final int current_index;
  const MyHomePage({super.key, required this.current_index});


  @override
  State<MyHomePage> createState() => _MyHomePageState();


}

class _MyHomePageState extends State<MyHomePage> {
  late int index;
  int count_of_orders = 0;

  @override
  void initState(){
    super.initState();
    index = widget.current_index;
    WidgetsBinding.instance.addPostFrameCallback((_) async{
      get_orders().then((value) => {
        setState((){
          count_of_orders = value.length;
          print(count_of_orders);
        })
      });
    });
  }

  Widget build(BuildContext context) {


    return Scaffold(
      body: [
        Center(child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(height: 200,),
            OutlinedButton(
              onPressed: (){
                Navigator.push(context, MaterialPageRoute(builder: (context) => Send_package_Page()));
              },
              child: Text("Send a package"),
            ),
            OutlinedButton(
                onPressed: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) => Notification_Page()));
                },
                child: Text("Notification"))
        ])),
        Wallet(),
        (count_of_orders == 0) ?Scaffold() : Tracking_Page(),
        ProfilePage()
      ][index],
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: index,
        iconSize: 24,
        selectedItemColor: Color(0xFF0560FA),
        unselectedItemColor: Color(0xFFA7A7A7),
        showUnselectedLabels: true,
        selectedFontSize: 12,
        unselectedFontSize: 12,
        onTap: (new_index){
          setState(() {
            index = new_index;
            print(index);
          });
        },
        items: [
          BottomNavigationBarItem(
              icon: Padding(
                padding: const EdgeInsets.only(bottom: 3),
                child: Image.asset((index == 0)
                    ? "assets/house-2_1.png"
                    : "assets/house-2.png"),
              ),
            label: "Home"
          ),
          BottomNavigationBarItem(
              icon: Padding(
                padding: const EdgeInsets.only(bottom: 3),
                child: Image.asset((index == 1)
                    ? "assets/wallet-3_1.png"
                    : "assets/wallet-3.png"),
              ),
              label: "Wallet"
          ),
          BottomNavigationBarItem(
              icon: Padding(
                padding: const EdgeInsets.only(bottom: 3),
                child: Image.asset((index == 2)
                    ? "assets/smart-car_1.png"
                    : "assets/smart-car.png"),
              ),
              label: "Track"
          ),
          BottomNavigationBarItem(
              icon: Padding(
                padding: const EdgeInsets.only(bottom: 3),
                child: Image.asset((index == 3)
                    ? "assets/profile-circle_1.png"
                    : "assets/profile-circle.png"),
              ),
              label: "Profile"
          ),

        ],
      ),

    );
  }
}
