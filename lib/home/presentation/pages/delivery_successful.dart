import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_star/star.dart';
import 'package:flutter_star/star_score.dart';
import 'package:sec_2_6/auth/data/repository/show_error.dart';
import 'package:sec_2_6/home/data/repository/supabase.dart';

import '../../../home/presentation/pages/home_page.dart';

class Delivery extends StatefulWidget {
  final String id;
  const Delivery({super.key, required this.id});


  @override
  State<Delivery> createState() => _DeliveryState();
}

class _DeliveryState extends State<Delivery> with SingleTickerProviderStateMixin{
  late AnimationController controller;
  TextEditingController controller_text = TextEditingController();
  bool is_Finish = false;
  int stars = 0;


  @override
  void initState(){
    controller = AnimationController(vsync: this, duration: Duration(milliseconds: 5000));
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    controller.forward().whenComplete(() => setState((){is_Finish = true;}));
    return Scaffold(
        body: SingleChildScrollView(
          padding: EdgeInsets.symmetric(horizontal: 24),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(height: 144,),
                (!is_Finish) ? RotationTransition(turns: Tween(begin: 1.0, end: 0.0).animate(controller),
                    child: Image.asset('assets/Component 21.png')): Image.asset("assets/Good Tick.png"),
                SizedBox(height: 219,),
                Text(
                    "Rate Rider",
                    style: TextStyle(color: Color(0xFF0560FA), fontSize: 14, fontWeight: FontWeight.w400)
                ),
                SizedBox(height: 16,),
                RatingBar.builder(
                  initialRating: 0,
                  minRating: 1,
                  direction: Axis.horizontal,
                  allowHalfRating: true,
                  itemCount: 5,
                  itemPadding: EdgeInsets.symmetric(horizontal: 16),
                  itemBuilder: (context, _) => Icon(
                    Icons.star,
                    color: Colors.amber,
                  ),
                  onRatingUpdate: (rating) {
                    setState(() {
                      stars = rating.round();
                    });
            },
          ),
                SizedBox(height: 36.60,),
                Container(
                  height: 50,
                  width: 342,
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                        color: Color(
                          0x26000000,
                        ),
                        offset: Offset(0, 2),
                        blurRadius: 5)
                  ], color: Colors.white),
                  child: TextField(
                    controller: controller_text,
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: 'Add feedback',
                        prefixIcon: Image.asset('assets/comment.png'),
                        hintStyle: TextStyle(
                          color: Color(0xFFA7A7A7),
                          fontSize: 12,
                          fontWeight: FontWeight.w400
                        )),
                  ),

                ),
                SizedBox(height: 141,),
                Align(
                  alignment: Alignment.center,
                  child: SizedBox(
                    height: 46,
                    width: 342,
                    child: OutlinedButton(
                        style: ButtonStyle(
                          backgroundColor: MaterialStatePropertyAll<Color>(Color(0xFF0560FA),),
                          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                                side: BorderSide(width: 0),
                                borderRadius: BorderRadius.circular(4),

                              )
                          ),
                        ),
                        onPressed: ()async{
                          await feedback_and_rate(stars, controller_text.text, widget.id, (){
                            Navigator.push(context, MaterialPageRoute(builder: (context) => MyHomePage(current_index: 0)));
                          }, (String e){showError(context, e);});


                        },
                        child: Text("Done", style: TextStyle(
                            fontSize: 16,
                            color: Colors.white,
                            fontWeight: FontWeight.w700
                        ),)),
                  ),
                )
              ],
            ))

    );
  }
}
