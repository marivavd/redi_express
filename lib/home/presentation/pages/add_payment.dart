import 'package:flutter/material.dart';
enum SingingCharacter { cash, card }
class Add_Payment_Page extends StatefulWidget {
  const Add_Payment_Page({super.key});

  @override
  State<Add_Payment_Page> createState() => _Add_Payment_PageState();
}

class _Add_Payment_PageState extends State<Add_Payment_Page> {



  @override
  Widget build(BuildContext context) {
  SingingCharacter? radio = SingingCharacter.cash;

    return Scaffold(
        body: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                height: 108,
                width: double.infinity,
                alignment: Alignment.bottomLeft,
                padding: const EdgeInsets.only(left: 14, right: 14, bottom: 19),
                decoration: const BoxDecoration(color: Colors.white, boxShadow: [
                  BoxShadow(
                      color: Color(0x26000026), blurRadius: 5, offset: Offset(0, 2))
                ]),
                child: Stack(children: [
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Text("Add Payment method",
                        style: Theme.of(context).textTheme.labelLarge),
                  ),
                  Align(alignment: Alignment.bottomLeft,
                      child: InkWell(
                        onTap: (){
                          Navigator.of(context).pop();
                        },
                        child: Image.asset("assets/arrow-square-right.png"),
                      ))
                ]),
              ),
              Padding(
                  padding: EdgeInsets.symmetric(horizontal: 24),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(color: Color(0x26000026), offset: Offset(0, 2))
                          ]
                      ),
                      child: ListTile(
                        contentPadding: EdgeInsets.symmetric(horizontal: 24),
                        leading: Radio<SingingCharacter>(
                            value: SingingCharacter.cash,
                            groupValue: radio,
                            onChanged: (value){
                              setState(() {
                                radio = value;
                              });
                            },
                        ),
                        title: Text("Pay with wallet",
                            style: TextStyle(
                              color: Color(0xFF3A3A3A),
                              fontSize: 16,
                              fontWeight: FontWeight.w400
                            )),
                        subtitle: Text("complete the payment using your e wallet", style: TextStyle(
                            color: Color(0xFFA7A7A7),
                            fontSize: 12,
                            fontWeight: FontWeight.w400
                        ),),
                      ),
                    ),
                    SizedBox(height: 12,),
                    Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(color: Color(0x26000026), offset: Offset(0, 2))
                          ]
                      ),
                      child: ListTile(
                        contentPadding: EdgeInsets.symmetric(horizontal: 24),
                        leading: Radio<SingingCharacter>(
                          value: SingingCharacter.card,
                          groupValue: radio,
                          onChanged: (value){
                            setState(() {
                              radio = value;
                            });
                          },
                        ),
                        title: Text("Credit / debit card",
                            style: TextStyle(
                                color: Color(0xFF3A3A3A),
                                fontSize: 16,
                                fontWeight: FontWeight.w400
                            )),
                        subtitle: Text("complete the payment using your debit card", style: TextStyle(
                            color: Color(0xFFA7A7A7),
                            fontSize: 12,
                            fontWeight: FontWeight.w400
                        ),),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),)
    );
  }
}
