import 'package:flutter/material.dart';
import 'package:sec_2_6/home/presentation/pages/add_payment.dart';

import '../../../auth/data/repository/show_error.dart';
import '../../data/repository/supabase.dart';
import '../widgets/itemtile.dart';



class ProfilePage extends StatefulWidget {
  const ProfilePage({super.key});

  @override
  State<ProfilePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<ProfilePage> {
  bool see_balance = true;
  String balance = "";
  String full_name = "";


  @override
  void initState(){
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async{
      getUser().then((value) => {
        setState((){
          full_name = value["fullname"].toString();
          balance = value["balance"].toString();
        })
      });
    });
  }
  @override
  Widget build(BuildContext context) {

    return Scaffold(
        body: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                height: 108,
                width: double.infinity,
                alignment: Alignment.bottomLeft,
                padding: const EdgeInsets.only(left: 14, right: 14, bottom: 19),
                decoration: const BoxDecoration(color: Colors.white, boxShadow: [
                  BoxShadow(
                      color: Color(0x26000026), blurRadius: 5, offset: Offset(0, 2))
                ]),
                child: Stack(children: [
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Text("Profile",
                        style: Theme.of(context).textTheme.labelLarge),
                  ),
                ]),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 24),
                child: Column(
                  children: [
                    SizedBox(height: 27,),
                    Row(
                      children: [
                        Expanded(
                            child: Row(
                              children: [
                                Image.asset('assets/Frame 83.png', width: 60, height: 60,),
                                SizedBox(width: 14,),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Hello $full_name',
                                      style: TextStyle(
                                          fontSize: 16,
                                          fontFamily: 'Roboto',
                                          fontWeight: FontWeight.w500,
                                          color: Color(0xFF3A3A3A)
                                      ),
                                    ),
                                    Row(
                                      children: [
                                        Text("Current balance: ", style: Theme.of(context).textTheme.titleSmall?.copyWith(color: Color(0xFF3A3A3A)),),
                                        (see_balance == true) ? Text("N${balance}:00", style: Theme.of(context).textTheme.titleSmall?.copyWith(color: Color(0xFF0560FA), fontWeight: FontWeight.w500),): Text("*******", style: Theme.of(context).textTheme.titleSmall?.copyWith(color: Color(0xFF0560FA), fontWeight: FontWeight.w500),),
                                      ],
                                    )

                                  ])
                              ],
                            )),
                        InkWell(
                          child: SizedBox(
                            height: 14,
                            width: 14,
                            child: Image.asset('assets/eye-slash.png'),
                          ),
                          onTap: (){
                            setState(() {
                              see_balance = !see_balance;
                            });

                          },
                        )


                      ],
                    ),
                    ItemTile(title: "Edit Profile", icon: "assets/iconoir_profile-circled.png", subtitle: "Name, phone no, address, email ...",),
                    SizedBox(height: 12,),
                    ItemTile(title: "Statements & Reports", icon: "assets/healthicons_i-certificate-paper-outline.png", subtitle: "Download transaction details, orders, deliveries",),
                    SizedBox(height: 12,),
                    ItemTile(title: "Notification Settings", icon: "assets/notification.png", subtitle: "mute, unmute, set location & tracking setting",),
                    SizedBox(height: 12,),
                    ItemTile(title: "Card & Bank account settings", icon: "assets/wallet-2.png", subtitle: "change cards, delete card details", ontap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context) => Add_Payment_Page()));
                    },),
                    SizedBox(height: 12,),
                    ItemTile(title: "Referrals", icon: "assets/carbon_two-person-lift.png", subtitle: "check no of friends and earn",),
                    SizedBox(height: 12,),
                    ItemTile(title: "About Us", icon: "assets/map.png", subtitle: "know more about us, terms and conditions",),
                    SizedBox(height: 12,),
                    ItemTile(title: "Log Out", icon: "assets/ic_round-log-out.png", ontap: (){
                      log_out(onResponse:(){},
                          onError: (String e){showError(context, e);});
                    },),
                    SizedBox(height: 12,),
                  ],
                ),

              )

            ],
          ),)
    );
  }
}
