import 'package:flutter/material.dart';
import 'package:sec_2_6/home/presentation/pages/send_package_2.dart';



import '../../data/repository/supabase.dart';
import '../widgets/text_field.dart';

class Send_package_Page extends StatefulWidget {
  const Send_package_Page({super.key});

  @override
  State<Send_package_Page> createState() => _Send_package_PageState();
}

class _Send_package_PageState extends State<Send_package_Page> {
  var address_controller = TextEditingController();
  var country_controller = TextEditingController();
  var phone_controller = TextEditingController();
  var others_controller = TextEditingController();
  var items_controller = TextEditingController();
  var weight_controller = TextEditingController();
  var worth_controller = TextEditingController();
  int count = 1;
  List<TextEditingController> adresses = [TextEditingController()];
  List<TextEditingController> countries = [TextEditingController()];
  List<TextEditingController> phones = [TextEditingController()];
  List<TextEditingController> otherses = [TextEditingController()];



  @override
  Widget build(BuildContext context) {

    return Scaffold(
        body: CustomScrollView(
          slivers: [
            SliverToBoxAdapter(
              child: Container(
                height: 108,
                width: double.infinity,
                alignment: Alignment.bottomLeft,
                padding: const EdgeInsets.only(left: 14, right: 14, bottom: 19),
                decoration: const BoxDecoration(color: Colors.white, boxShadow: [
                  BoxShadow(
                      color: Color(0x26000026), blurRadius: 5, offset: Offset(0, 2))
                ]),
                child: Stack(children: [
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Text("Send a package",
                        style: Theme.of(context).textTheme.labelLarge),
                  ),
                  Align(alignment: Alignment.bottomLeft,
                      child: InkWell(
                        onTap: (){
                          Navigator.of(context).pop();
                        },
                        child: Image.asset("assets/arrow-square-right.png"),
                      ))
                ]),
              ),
            ),
            SliverToBoxAdapter(child: SizedBox(height: 32)),
            SliverToBoxAdapter(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 24),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Image.asset("assets/Frame 60.png"),
                        SizedBox(width: 8,),
                        Text("Origin Details",
                          style: Theme.of(context).textTheme.labelMedium,)
                      ],
                    ),
                    SizedBox(height: 5,),
                    Custom_Field(hint: "Address", controller: address_controller),
                    SizedBox(height: 5,),
                    Custom_Field(hint: "State,Country", controller: country_controller),
                    SizedBox(height: 5,),
                    Custom_Field(hint: "Phone number", controller: phone_controller),
                    SizedBox(height: 5,),
                    Custom_Field(hint: "Others", controller: others_controller),
                  ],
                ),
              ),
            ),
            SliverList.builder(
              itemCount: count,
              itemBuilder: (_, index){
                return Padding(
                  padding: EdgeInsets.symmetric(horizontal: 24),
                  child: Container(
                    child: Column(
                      children: [
                        SizedBox(height: 39,),
                        Row(
                          children: [
                            Image.asset("assets/Frame 59.png"),
                            SizedBox(width: 8,),
                            Text("Destination Details",
                              style: Theme.of(context).textTheme.labelMedium,)
                          ],
                        ),
                        SizedBox(height: 5,),
                        Custom_Field(hint: "Address", controller: adresses[index]),
                        SizedBox(height: 5,),
                        Custom_Field(hint: "State,Country", controller: countries[index]),
                        SizedBox(height: 5,),
                        Custom_Field(hint: "Phone number", controller: phones[index]),
                        SizedBox(height: 5,),
                        Custom_Field(hint: "Others", controller: otherses[index]),
                        SizedBox(height: 10,),
                      ],
                    ),
                  ),
                );
              },
            ),
            SliverToBoxAdapter(
                child:  Padding(
                  padding: EdgeInsets.symmetric(horizontal: 24),
                  child: Row(
                    children: [
                      InkWell(
                        onTap: (){
                          setState(() {
                            count ++;
                            adresses.add(TextEditingController());
                            phones.add(TextEditingController());
                            countries.add(TextEditingController());
                            otherses.add(TextEditingController());
                          });
                        },
                        child: Image.asset("assets/add-square.png"),
                      ),

                      SizedBox(width: 2,),
                      Text("Add destination",
                        style: TextStyle(
                            fontSize: 12,
                            color: Color(0xFFA7A7A7),
                            fontWeight: FontWeight.w400
                        ),)
                    ],
                  ),
                )
            ),
            SliverToBoxAdapter(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 24),
                child: Column(
                  children: [
                    SizedBox(height: 13,),
                    Row(children: [Text("Package Details", style: Theme.of(context).textTheme.labelMedium,),],),
                    SizedBox(height: 5,),
                    Custom_Field(hint: "package items", controller: items_controller),
                    SizedBox(height: 5,),
                    Custom_Field(hint: "Weight of item(kg)", controller: weight_controller),
                    SizedBox(height: 5,),
                    Custom_Field(hint: "Worth of Items", controller: worth_controller),
                    SizedBox(height: 39,),
                    Row(children: [Text("Select delivery type", style: Theme.of(context).textTheme.labelMedium,),],),
                    Row(
                      children: [
                        Align(
                          alignment: Alignment.bottomLeft,
                            child: InkWell(
                              onTap: ()async{
                                double delivery_charges = adresses.length * 2500;
                                double instantDelivery = 300.00;
                                double tax = (delivery_charges + instantDelivery) * 0.05;
                                double sum_price = tax + instantDelivery + delivery_charges;
                                List<Map<String, String>> destinations = [];
                                for (int i = 0; i < count; i++){
                                  destinations.add(
                                    {
                                      "address": adresses[i].text,
                                      "country": countries[i].text,
                                      "phone": phones[i].text,
                                      "others": otherses[i].text
                                    }
                                  );
                                }
                                String id = await insertNewOrder(
                                    address_controller.text,
                                    country_controller.text,
                                    phone_controller.text,
                                    others_controller.text,
                                    items_controller.text,
                                    int.parse(weight_controller.text),
                                    int.parse(worth_controller.text),
                                    delivery_charges,
                                    instantDelivery,
                                    tax,
                                    sum_price,
                                    destinations);
                                Navigator.push(context, MaterialPageRoute(builder: (context) => Send_package_2_Page(id: id, destinations: destinations, address: address_controller.text, country: country_controller.text, phone: phone_controller.text, others: others_controller.text, items: items_controller.text, weight: weight_controller.text, worth: worth_controller.text, delivery_charges: delivery_charges.toString(), instantDelivery: instantDelivery.toString(), tax: tax.toString(), sum_price: sum_price.toString())));
                              },
                              child: Image.asset("assets/Frame 78.png"),
                            )
                        ),
                        SizedBox(width: 24,),
                        Align(
                            alignment: Alignment.bottomRight,
                            child: InkWell(
                              onTap: (){},
                              child: Image.asset("assets/Frame 82.png"),
                            )
                        ),
                      ],
                    )

                  ],
                ),
              ),
            )

          ],
          ),
    );
  }
}
