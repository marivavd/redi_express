import 'package:flutter/material.dart';

import '../../../home/presentation/pages/home_page.dart';

class Transition extends StatefulWidget {
  final String id;
  const Transition({super.key, required this.id});


  @override
  State<Transition> createState() => _TransitionState();
}

class _TransitionState extends State<Transition> with SingleTickerProviderStateMixin{
  late AnimationController controller;
  bool is_Finish = false;

  @override
  void initState(){
    controller = AnimationController(vsync: this, duration: Duration(milliseconds: 5000));
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    controller.forward().whenComplete(() => setState((){is_Finish = true;}));
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 24),
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(height: 144,),
          (!is_Finish) ? RotationTransition(turns: Tween(begin: 1.0, end: 0.0).animate(controller),
          child: Image.asset('assets/Component 21.png')): Image.asset("assets/Good Tick.png"),
          SizedBox(height: 130,),
          Text(
            "Your rider is on the way to your destination",
              style: Theme.of(context).textTheme.labelMedium?.copyWith(fontWeight: FontWeight.w400)
          ),
          SizedBox(height: 8,),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "Tracking Number ",
                  style: Theme.of(context).textTheme.labelMedium?.copyWith(fontWeight: FontWeight.w400)
              ),
              Text(
                (widget.id.length <= 21) ? "R-${widget.id}" : "${"R-${widget.id}".substring(0, 18)}...",

                style: Theme.of(context).textTheme.labelMedium?.copyWith(fontWeight: FontWeight.w400, color: Color(0xFF0560FA)),
              ),
            ],
          ),
          SizedBox(height: 141,),
          SizedBox(
            height: 46,
            width: 342,
            child: OutlinedButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStatePropertyAll<Color>(Color(0xFF0560FA),),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                        side: BorderSide(width: 0),
                        borderRadius: BorderRadius.circular(4),

                      )
                  ),
                ),
                onPressed: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) => MyHomePage(current_index: 2)));
                },
                child: Text("Track my item", style: TextStyle(
                    fontSize: 16,
                    color: Colors.white,
                    fontWeight: FontWeight.w700
                ),)),
          ),
          SizedBox(height: 8,),
          SizedBox(
            height: 46,
            width: 342,
            child: OutlinedButton(
                style: ButtonStyle(
                  side: MaterialStateProperty.all(
                      BorderSide(
                        color: Color(0xFF0560FA),
                        width: 1.0,
                        style: BorderStyle.solid,)),
                  backgroundColor: MaterialStatePropertyAll<Color>(Colors.white,),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(4),

                      ),
                  ),
                ),
                onPressed: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) => MyHomePage(current_index: 0)));
                },
                child: Text("Go back to homepage", style: TextStyle(
                    fontSize: 16,
                    color: Color(0xFF0560FA),
                    fontWeight: FontWeight.w700
                ),)),
          )
        ],
      ))

    );
  }
}
