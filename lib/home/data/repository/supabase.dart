import 'package:supabase_flutter/supabase_flutter.dart';

var supabase = Supabase.instance.client;


Future<void> log_out(
{
  required Function onResponse,
  required Function onError}) async{
  try{
    await supabase.auth.signOut();
    onResponse();
  }
  on AuthException catch(e){
    onError(e.message);
  }
  on Exception catch(e){
    onError("Something went wrong");
  }
}
Future<void> feedback_and_rate(stars, feedback, id, onResponce, onError) async{
  try{
  await supabase
      .from('orders')
      .update({'feedback': feedback, 'rate': stars}).match({"id": id});
  onResponce();}
  on AuthException catch(e){
    onError(e.message);
  }
  on Exception catch(e){
    onError("Something went wrong");
  }

}
Future<Map<String, dynamic>> getUser() async{
  return await supabase.from("profiles").select().eq("id_user", supabase.auth.currentUser!.id).single();
}
Future<List<Map<String, dynamic>>> getTransactions() async{
  return await supabase.from("transactions").select().eq("id_user", supabase.auth.currentUser!.id);
}
Future<Map<String, dynamic>> getOrder() async{
  return await supabase.from("orders").select().eq("id_user", supabase.auth.currentUser!.id).single();
}
Future<List<Map<String, dynamic>>> get_orders() async{
  return await supabase.from("orders").select().eq("id_user", supabase.auth.currentUser!.id);
}
Future<List<Map<String, dynamic>>> getDestinationDetailsOrder(id_order) async{
  return await supabase.from("destination_details").select().eq("id_order", id_order);
}
Future<String> insertNewOrder(
    String address,
    String country,
    String phone,
    String others,
    String items,
    int weight,
    int worth,
    double delivery_charges,
    double instantDelivery,
    double tax,
    double sum_price,
    List<Map<String, String>> destinations) async {
  String idOrder = await supabase
      .from("orders")
      .insert({
    "id_user": supabase.auth.currentUser!.id,
    "address": address,
    "country": country,
    "phone": phone,
    "others": others,
    "package_items": items,
    "weight_items": weight,
    "worth_items": worth,
    "delivery_charges": delivery_charges,
    "instant_delivery": instantDelivery,
    "tax_and_service_charges": tax,
    "sum_price": sum_price
  })
      .select()
      .single()
      .then((value) => value["id"]);

  for (var i in destinations) {
    i["id_order"] = idOrder;
    await supabase.from("destinations_details").insert(i);
  }
  return idOrder;
}
Future<void> subscribeOrder(String orderId, callback) async {
  supabase
      .channel("orders-status-changes")
      .onPostgresChanges(
      event: PostgresChangeEvent.update,
      schema: "public",
      table: "orders",
      filter: PostgresChangeFilter(
          type: PostgresChangeFilterType.eq,
          column: "id",
          value: orderId
      ),
      callback: (payload) {
        callback(payload.newRecord);
      }
  )
      .subscribe();
}