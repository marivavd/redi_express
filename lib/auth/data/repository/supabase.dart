import 'package:email_validator/email_validator.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

final supabase = Supabase.instance.client;

Future<void> signUp(
    {required String email,
    required String phone,
    required String full_name,
    required String password,
    required String confirm_password,
    required Function onResponse,
    required Function onError}) async {
  try{
    if(confirm_password != password){
      onError('Passwords do not match');
    }
    else{
      if (!EmailValidator.validate(email)){
        onError('Incorrect email');
      }
      else{
        final AuthResponse res = await supabase.auth.signUp(
          email: email,
          password: password,
        );
        await supabase.from('profiles').insert({
          "id_user": res.user!.id,
          "fullname": full_name,
          'phone': phone,
          'avatar': ''
        });
        onResponse();
      }
    }
  }
  on AuthException catch(e){
    onError(e.message);
  }
  on Exception catch(e){
    onError("Something is wrong");
  }
}
Future<void> signIn(
    {required String email,
      required String password,
      required Function onResponse,
      required Function onError}) async {
  try{
        final AuthResponse res = await supabase.auth.signInWithPassword(
          email: email,
          password: password,
        );
        onResponse();
      }
  on AuthException catch(e){
    onError(e.message);
  }
  on Exception catch(e){
    onError("Something is wrong");
  }
}
Future<void> sendEmail(
    {required String email,
      required Function onResponse,
      required Function onError}) async {
  try{
    await supabase.auth.resetPasswordForEmail(email);
    onResponse();
  }
  on AuthException catch(e){
    onError(e.message);
  }
  on Exception catch(e){
    onError("Something is wrong");
  }
}
Future<void> changePassword(
    {required String new_password,
      required Function onResponse,
      required Function onError}) async {
  try{
    await supabase.auth.updateUser(UserAttributes(password: new_password));
    onResponse();
  }
  on AuthException catch(e){
    onError(e.message);
  }
  on Exception catch(e){
    onError("Something is wrong");
  }
}
Future<void> verifyOTP(
    {required String email,
      required String code,
      required Function onResponse,
      required Function onError}) async {
  try{
    await supabase.auth.verifyOTP(
      type: OtpType.email,
      token: code,
      email: email
    );
    onResponse();
  }
  on AuthException catch(e){
    onError(e.message);
  }
  on Exception catch(e){
    onError("Something is wrong");
  }
}