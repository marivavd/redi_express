
import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class Custom_Field extends StatelessWidget{
  final String label;
  final String hint;
  final TextEditingController controller;
  final Function(String?) onchange;
  final bool is_obscure;
  final Function()? tap_suffix;
  final MaskTextInputFormatter? formatter;
  const Custom_Field({required this.label, required this.hint, required this.controller, required this.onchange, this.is_obscure = false, this.formatter, this.tap_suffix});



  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          label,
          style: Theme.of(context).textTheme.titleSmall,
        ),
        const SizedBox(height: 8,),
        TextField(
          onChanged: onchange,
          controller: controller,
          inputFormatters:(formatter != null) ? [formatter!] : [],
          obscureText: is_obscure,
          obscuringCharacter: '*',
          decoration: InputDecoration(
              contentPadding: const EdgeInsets.symmetric(vertical: 14, horizontal: 10),
            hintText: hint,
            hintStyle: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w500,
                color: Color(0xFFCFCFCF),
                fontFamily: 'Roboto'
            ),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(4),
              borderSide: BorderSide(
                color: Color(0xFFA7A7A7),
                width: 4,
              )
            ),
            suffixIcon: (tap_suffix != null)
                ? GestureDetector(
              onTap: tap_suffix,
              child: Image.asset("assets/eye-slash.png"),
            )
                : null,
          ),
        )
      ],
    );
  }
}