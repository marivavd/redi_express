
import 'package:flutter/material.dart';
import 'package:sec_2_6/auth/data/repository/show_error.dart';
import 'package:sec_2_6/auth/data/repository/supabase.dart';
import 'package:sec_2_6/auth/presentation/pages/forgot_password.dart';
import 'package:sec_2_6/auth/presentation/pages/sign_up_page.dart';
import 'package:sec_2_6/auth/presentation/widgets/text_field.dart';
import 'package:sec_2_6/home/presentation/pages/home_page.dart';

class Sign_in_Page extends StatefulWidget {
  const Sign_in_Page({super.key});
  @override
  State<Sign_in_Page> createState() => _Sign_in_PageState();
}

class _Sign_in_PageState extends State<Sign_in_Page> {
  var email_controller = TextEditingController();
  var password_controller = TextEditingController();
  bool password_obscure = true;
  bool button = false;
  bool check = false;
  void is_Valid(){
    setState(() {
      button = email_controller.text.isNotEmpty && password_controller.text.isNotEmpty;
    });
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 131,
              ),
              Text(
                "Welcome Back",
                style: Theme.of(context).textTheme.titleLarge,
              ),
              SizedBox(height: 8,),
              Text(
                "Fill in your email and password to continue",
                style: Theme.of(context).textTheme.titleSmall,
              ),
              SizedBox(
                height: 20,
              ),
              Custom_Field(label: "Email Address", hint: "***********@mail.com", controller: email_controller, onchange: (new_text){is_Valid();}),
              SizedBox(height: 24,),
              Custom_Field(label: "Password", hint: "**********", controller: password_controller, onchange: (new_text){is_Valid();}, is_obscure: password_obscure, tap_suffix: (){
                setState(() {
                  password_obscure =!password_obscure;
                });
              },),
              SizedBox(height: 17,),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(width: 1,),
                  Expanded(
                      child: Row(
                        children: [
                          SizedBox(
                            height: 14,
                            width: 14,
                            child: Checkbox(
                                value: check,
                                side: BorderSide(color: Color(0xFFA7A7A7), width: 1),
                                activeColor: Color.fromARGB(255, 0, 108, 236),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(2)
                                ),
                                onChanged: (bool? val){
                                  setState(() {
                                    check = val!;
                                    is_Valid();
                                  });
                                }),
                          ),
                          SizedBox(width: 4,),
                          Text(
                            "Remember password",
                            style: Theme.of(context).textTheme.titleSmall?.copyWith(fontSize: 12),
                          )
                        ],
                      )
                  ),
                  InkWell(
                    child: Text("Forgot Password?", style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                        fontFamily: 'Roboto',
                        color: Color.fromARGB(255, 5, 96, 250)
                    )),
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context) => Forgot_password_Page()));
                    },
                  )
                ],
              ),
              SizedBox(
                height: 187,
              ),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  height: 46,
                  width: 342,
                  child: FilledButton(
                    child: Text(
                      "Log In",
                      style: Theme.of(context).textTheme.titleMedium,
                    ),
                    style: Theme.of(context).filledButtonTheme.style,
                    onPressed: (button)? ()async{
                      signIn(
                          email: email_controller.text,
                          password: password_controller.text,
                          onResponse: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context) => MyHomePage(current_index: 0,)));
                          },
                          onError: (String e){showError(context, e);});
                    }: null,
                  ),
                ),
              ),
              SizedBox(height: 20,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("Already have an account?",
                    style: Theme.of(context).textTheme.titleSmall?.copyWith(fontWeight: FontWeight.w400),),
                  InkWell(
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context) => Sign_up_Page()));
                    },
                    child: Text(
                      "Sign Up",
                      style: Theme.of(context).textTheme.titleSmall?.copyWith(color: Color(0xFF0560FA)),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 18,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Text("or sign in using",
                        style: Theme.of(context).textTheme.titleSmall?.copyWith(fontWeight: FontWeight.w400),),
                      SizedBox(height: 8,),
                      Image.asset("assets/Facebook google, apple.png", width: 16, height: 16,)
                    ],
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
