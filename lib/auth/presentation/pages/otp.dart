import 'package:flutter/material.dart';
import 'package:pinput/pinput.dart';
import 'package:sec_2_6/auth/data/repository/show_error.dart';
import 'package:sec_2_6/auth/data/repository/supabase.dart';
import 'package:sec_2_6/auth/presentation/pages/new_password.dart';
import 'package:sec_2_6/auth/presentation/pages/sign_in_page.dart';
import 'package:sec_2_6/auth/presentation/pages/sign_up_page.dart';
import 'package:sec_2_6/auth/presentation/widgets/text_field.dart';
import 'package:sec_2_6/home/presentation/pages/home_page.dart';

class OTP extends StatefulWidget {
  final String email;
  const OTP({super.key, required this.email});
  @override
  State<OTP> createState() => _OTPState();
}

class _OTPState extends State<OTP> {
  var controller = TextEditingController();
  bool button = false;
  var is_Error = false;
  @override
  Widget build(BuildContext context) {
    double widthScreen = MediaQuery.of(context).size.width;
    double separator = widthScreen / 6 - 32;

    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 131,
              ),
              Text(
                "OTP Verification",
                style: Theme.of(context).textTheme.titleLarge,
              ),
              SizedBox(height: 8,),
              Text(
                "Enter the 6 digit numbers sent to your email",
                style: Theme.of(context).textTheme.titleSmall,
              ),
              SizedBox(
                height: 56,
              ),
              Pinput(
                length: 6,
                controller: controller,
                separatorBuilder: (context) => SizedBox(width: separator,),
                defaultPinTheme: PinTheme(
                  width: 32,
                  height: 32,
                  decoration: BoxDecoration(
                    border: Border.all(color: Color(0xFFA7A7A7)),
                    borderRadius: BorderRadius.zero
                  )
                ),
                focusedPinTheme: PinTheme(
                    width: 32,
                    height: 32,
                    decoration: BoxDecoration(
                        border: Border.all(color: Color(0xFF0560FA)),
                        borderRadius: BorderRadius.zero
                    )
                ),
                submittedPinTheme: (!is_Error) ? PinTheme(
                    width: 32,
                    height: 32,
                    decoration: BoxDecoration(
                        border: Border.all(color: Color(0xFF0560FA)),
                        borderRadius: BorderRadius.zero
                    )
                ): PinTheme(
                    width: 32,
                    height: 32,
                    decoration: BoxDecoration(
                        border: Border.all(color: Color(0xFFED3A3A)),
                        borderRadius: BorderRadius.zero
                    )
                ),
                onChanged: (text){
                  setState(() {
                    is_Error = false;
                    button = text.length == 6;
                  });
                },

              ),
              SizedBox(height: 30,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("If you didn’t receive code, ", style: Theme.of(context).textTheme.titleSmall?.copyWith(fontWeight: FontWeight.w400),),
                  InkWell(
                    onTap: ()async{
                      await sendEmail(
                      email: widget.email,
                      onResponse: (){
                      },
                      onError: (String e){showError(context, e);});
                    },
                    child: Text(
                      "resend",
                      style: Theme.of(context).textTheme.titleSmall?.copyWith(color: Color(0xFF0560FA)),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 56,),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  height: 46,
                  width: 342,
                  child: FilledButton(
                    child: Text(
                      "Set New Password",
                      style: Theme.of(context).textTheme.titleMedium,
                    ),
                    style: Theme.of(context).filledButtonTheme.style,
                    onPressed: (button)? ()async{
                      verifyOTP(
                          email: widget.email,
                          code: controller.text,
                          onResponse: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context) => New_password_Page()));
                          },
                          onError: (String e){
                            setState(() {
                              is_Error = true;
                            });
                            showError(context, e);});
                    }: null,
                  ),
                ),
              ),
              SizedBox(height: 20,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("Remember password? Back to ",
                    style: Theme.of(context).textTheme.titleSmall?.copyWith(fontWeight: FontWeight.w400),),
                  InkWell(
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context) => Sign_in_Page()));
                    },
                    child: Text(
                      "Sign In",
                      style: Theme.of(context).textTheme.titleSmall?.copyWith(color: Color(0xFF0560FA)),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 18,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Text("or sign in using",
                        style: Theme.of(context).textTheme.titleSmall?.copyWith(fontWeight: FontWeight.w400),),
                      SizedBox(height: 8,),
                      Image.asset("assets/Facebook google, apple.png", width: 16, height: 16,)
                    ],
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
