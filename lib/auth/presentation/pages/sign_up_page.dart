import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/link.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:sec_2_6/auth/data/repository/supabase.dart';
import 'package:sec_2_6/auth/presentation/pages/sign_in_page.dart';
import 'package:sec_2_6/auth/presentation/widgets/text_field.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

import '../../data/repository/show_error.dart';

class Sign_up_Page extends StatefulWidget {
  const Sign_up_Page({super.key});
  @override
  State<Sign_up_Page> createState() => _Sign_up_PageState();
}

class _Sign_up_PageState extends State<Sign_up_Page> {
  var name_controller = TextEditingController();
  var phone_controller = MaskedTextController(mask: "+0(000)000-00-00");
  var email_controller = TextEditingController();
  var password_controller = TextEditingController();
  var confirm_password_controller = TextEditingController();
  var password_obscure = true;
  var confirm_password_obscure = true;
  bool button = false;
  bool check = false;
  void is_Valid(){
    setState(() {
      button = name_controller.text.isNotEmpty && phone_controller.text.isNotEmpty && email_controller.text.isNotEmpty && password_controller.text.isNotEmpty && confirm_password_controller.text.isNotEmpty && check;
    });
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 54,
              ),
              Text(
                "Create an account",
                style: Theme.of(context).textTheme.titleLarge,
              ),
              SizedBox(height: 8,),
              Text(
                "Complete the sign up process to get started",
                style: Theme.of(context).textTheme.titleSmall,
              ),
              SizedBox(
                height: 33,
              ),
              Custom_Field(label: "Full name", hint: "Ivanov Ivan", controller: name_controller, onchange: (new_text){is_Valid();}),
              SizedBox(height: 24,),
              Custom_Field(label: "Phone Number", hint: "+7(999)999-99-99", controller: phone_controller, onchange: (new_text){is_Valid();}),
              SizedBox(height: 24,),
              Custom_Field(label: "Email Address", hint: "***********@mail.com", controller: email_controller, onchange: (new_text){is_Valid();}),
              SizedBox(height: 24,),
              Custom_Field(label: "Password", hint: "**********", controller: password_controller, onchange: (new_text){is_Valid();}, is_obscure: password_obscure, tap_suffix: (){
                setState(() {
                  password_obscure =!password_obscure;
                });
              },),
              SizedBox(height: 24,),
              Custom_Field(label: "Confirm Password", hint: "**********", controller: confirm_password_controller, onchange: (new_text){is_Valid();}, is_obscure: confirm_password_obscure, tap_suffix: (){
                setState(() {
                  confirm_password_obscure = !confirm_password_obscure;
                });
              },),
              SizedBox(height: 37,),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(width: 1,),
                  SizedBox(
                    height: 14,
                    width: 14,
                    child: Checkbox(
                        value: check,
                        side: BorderSide(color: Color(0xFF006CEC), width: 1),
                        activeColor: Color.fromARGB(255, 0, 108, 236),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(2)
                        ),
                        onChanged: (bool? val){
                          setState(() {
                            check = val!;
                            is_Valid();
                          });
                        }),
                  ),
                  SizedBox(width: 11,),
                  Expanded(
                      child: Padding(
                        padding: EdgeInsets.only(right: 69),
                      child: GestureDetector(
                        onTap: ()async{
                          final Uri url = Uri.parse("https://uboklrrvwysdoztwfvuj.supabase.co/storage/v1/object/public/profiles/tutorcoon%20(1).docx?t=2024-02-13T19%3A43%3A22.210Z");
                          await launchUrl(url, mode: LaunchMode.externalApplication);
                        },
                          child: RichText(
                    textAlign: TextAlign.center,
                      text: TextSpan(
                        text: "By ticking this box, you agree to our ",
                        style: TextStyle(
                          color: Color(0xFFA7A7A7),
                          fontSize: 12,
                          fontWeight: FontWeight.w400
                        ),
                        children: [
                          TextSpan(
                              text: "Terms and \nconditions and private policy",
                              style: TextStyle(
                                color: Color(0xFFEBBC2E)
                              ))
                        ]
                      ),
                  ))))
                ],
              ),
              SizedBox(
                height: 64,
              ),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  height: 46,
                  width: 342,
                  child: FilledButton(
                    child: Text(
                      "Sign Up",
                      style: Theme.of(context).textTheme.titleMedium,
                    ),
                    style: Theme.of(context).filledButtonTheme.style,
                    onPressed: (button)? ()async{
                      signUp(
                          email: email_controller.text,
                          phone: phone_controller.text,
                          full_name: name_controller.text,
                          password: password_controller.text,
                          confirm_password: confirm_password_controller.text,
                          onResponse: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context) => Sign_in_Page()));
                          },
                          onError: (String e){showError(context, e);});
                    }: null,
                  ),
                ),
              ),
              SizedBox(height: 20,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("Already have an account?",
                  style: Theme.of(context).textTheme.titleSmall?.copyWith(fontWeight: FontWeight.w400),),
                  InkWell(
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context) => Sign_in_Page()));
                    },
                    child: Text(
                      "Sign In",
                      style: Theme.of(context).textTheme.titleSmall?.copyWith(color: Color(0xFF0560FA)),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 18,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Text("or sign in using",
                      style: Theme.of(context).textTheme.titleSmall?.copyWith(fontWeight: FontWeight.w400),),
                      SizedBox(height: 8,),
                      Image.asset("assets/Facebook google, apple.png", width: 16, height: 16,)
                    ],
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
