import 'package:flutter/material.dart';
import 'package:sec_2_6/auth/data/repository/show_error.dart';
import 'package:sec_2_6/auth/data/repository/supabase.dart';
import 'package:sec_2_6/auth/presentation/pages/forgot_password.dart';
import 'package:sec_2_6/auth/presentation/pages/sign_up_page.dart';
import 'package:sec_2_6/auth/presentation/widgets/text_field.dart';
import 'package:sec_2_6/home/presentation/pages/home_page.dart';

class New_password_Page extends StatefulWidget {
  const New_password_Page({super.key});
  @override
  State<New_password_Page> createState() => _New_password_PageState();
}

class _New_password_PageState extends State<New_password_Page> {
  var confirm_password_controller = TextEditingController();
  var password_controller = TextEditingController();
  bool password_obscure = true;
  bool confirm_password_obscure = true;
  bool button = false;
  void is_Valid(){
    setState(() {
      button = confirm_password_controller.text.isNotEmpty && password_controller.text.isNotEmpty;
    });
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 131,
              ),
              Text(
                "New Password",
                style: Theme.of(context).textTheme.titleLarge,
              ),
              SizedBox(height: 8,),
              Text(
                "Enter new password",
                style: Theme.of(context).textTheme.titleSmall,
              ),
              SizedBox(
                height: 70,
              ),
              Custom_Field(label: "Password", hint: "**********", controller: password_controller, onchange: (new_text){is_Valid();}, is_obscure: password_obscure, tap_suffix: (){
                setState(() {
                  password_obscure =!password_obscure;
                });
              },),
              SizedBox(height: 24,),
              Custom_Field(label: "Confirm Password", hint: "**********", controller: confirm_password_controller, onchange: (new_text){is_Valid();}, is_obscure: confirm_password_obscure, tap_suffix: (){
                setState(() {
                  confirm_password_obscure =!confirm_password_obscure;
                });
              },),
              SizedBox(height: 71,),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  height: 46,
                  width: 342,
                  child: FilledButton(
                    child: Text(
                      "Log In",
                      style: Theme.of(context).textTheme.titleMedium,
                    ),
                    style: Theme.of(context).filledButtonTheme.style,
                    onPressed: (button)? ()async{
                      if (password_controller.text == confirm_password_controller.text){
                        changePassword(
                            new_password: password_controller.text,
                            onResponse: (){
                              Navigator.push(context, MaterialPageRoute(builder: (context) => MyHomePage(current_index: 0,)));
                            },
                            onError: (String e){showError(context, e);});
                      }
                    }: null,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
