import 'package:flutter/material.dart';
import 'package:sec_2_6/auth/data/repository/show_error.dart';
import 'package:sec_2_6/auth/data/repository/supabase.dart';
import 'package:sec_2_6/auth/presentation/pages/otp.dart';
import 'package:sec_2_6/auth/presentation/pages/sign_in_page.dart';
import 'package:sec_2_6/auth/presentation/pages/sign_up_page.dart';
import 'package:sec_2_6/auth/presentation/widgets/text_field.dart';
import 'package:sec_2_6/home/presentation/pages/home_page.dart';

class Forgot_password_Page extends StatefulWidget {
  const Forgot_password_Page({super.key});
  @override
  State<Forgot_password_Page> createState() => _Forgot_password_PageState();
}

class _Forgot_password_PageState extends State<Forgot_password_Page> {
  var email_controller = TextEditingController();
  bool button = false;
  void is_Valid(){
    setState(() {
      button = email_controller.text.isNotEmpty;
    });
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 131,
              ),
              Text(
                "Forgot Password",
                style: Theme.of(context).textTheme.titleLarge,
              ),
              SizedBox(height: 8,),
              Text(
                "Enter your email address",
                style: Theme.of(context).textTheme.titleSmall,
              ),
              SizedBox(
                height: 56,
              ),
              Custom_Field(label: "Email Address", hint: "***********@mail.com", controller: email_controller, onchange: (new_text){is_Valid();}),
              SizedBox(height: 56,),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  height: 46,
                  width: 342,
                  child: FilledButton(
                    child: Text(
                      "Send OTP",
                      style: Theme.of(context).textTheme.titleMedium,
                    ),
                    style: Theme.of(context).filledButtonTheme.style,
                    onPressed: (button)? ()async{
                      await sendEmail(
                          email: email_controller.text,
                          onResponse: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context) => OTP(email: email_controller.text)));
                          },
                          onError: (String e){showError(context, e);});
                    }: null,
                  ),
                ),
              ),
              SizedBox(height: 20,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("Remember password? Back to ",
                    style: Theme.of(context).textTheme.titleSmall?.copyWith(fontWeight: FontWeight.w400),),
                  InkWell(
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context) => Sign_in_Page()));
                    },
                    child: Text(
                      "Sign In",
                      style: Theme.of(context).textTheme.titleSmall?.copyWith(color: Color(0xFF0560FA)),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 18,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Text("or sign in using",
                        style: Theme.of(context).textTheme.titleSmall?.copyWith(fontWeight: FontWeight.w400),),
                      SizedBox(height: 8,),
                      Image.asset("assets/Facebook google, apple.png", width: 16, height: 16,)
                    ],
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
